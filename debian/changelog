editmoin (1.17-5) unstable; urgency=medium

  * Bump Standards-Version to 4.4.0.  No changes necessary.
  * Orphan package. See #939054

 -- Martin Pitt <mpitt@debian.org>  Sun, 01 Sep 2019 10:25:10 +0000

editmoin (1.17-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Martin Pitt ]
  * Add Recommends sensible-utils.
    If $EDITOR is not set, sensible-editor is the default.
  * Mark as Multi-Arch: foreign
  * Move Homepage: to https:// URL
  * Bump Standards-Version to 4.3.0

 -- Martin Pitt <mpitt@debian.org>  Sun, 20 Jan 2019 14:46:38 +0000

editmoin (1.17-3) unstable; urgency=medium

  * Move Vcs-* to salsa
  * debian/copyright: Move to https URLs
  * Move to debhelper compat level 10
  * Bump Standards-Version to 4.1.4

 -- Martin Pitt <mpitt@debian.org>  Tue, 01 May 2018 11:39:33 +0000

editmoin (1.17-2) unstable; urgency=medium

  * Move packaging to collab-maint git, update Vcs-* accordingly
  * Bump debhelper compat to 9
  * Annotate python dependency with ":any" M-A qualifier
  * debian/copyright: Move to machine parseable copyright format 1.0
  * Bump Standards-Version to 3.9.8
  * Manage/reformat patches with gbp pq

 -- Martin Pitt <mpitt@debian.org>  Wed, 23 Nov 2016 12:41:19 +0100

editmoin (1.17-1) unstable; urgency=low

  * New upstream release:
    - Use sensible-editor instead of vi to pick up the system level editor
      preference.
    - Add support for URLs that include query parameters.
  * Switch to 3.0 source package format.
  * Move inline change to editmoin.1 to 01_man_ids_from_firefox.patch. Also
    update it to catch cookies like "MOIN_SESSION_443_ROOT".
  * Move moin-plugin.vim into debian/, to work better for 3.0 source.
  * debian/control: Bump Standards-Version to 3.9.2.
  * Add 02_alternative_cookie_names.patch: Current moin versions use
    MOIN_SESSION_80_ROOT or MOIN_SESSION_443_ROOT cookie names. Try these
    before the standard MOIN_SESSION cookie. Thanks Andy Whitcroft for the
    patch! (LP: #801284)
  * debian/editmoin.install, debian/rules: Simplify installation of vim
    plugin.
  * Add bzr-builddeb configuration (merge mode).

 -- Martin Pitt <mpitt@debian.org>  Wed, 06 Jul 2011 07:04:53 +0200

editmoin (1.16-1) unstable; urgency=low

  * New upstream release:
    - Split editing logic in its own function
    - avoid showing the note message multiple times
    - allow arguments in $EDITOR variable
  * editmoin.1: Add example command how to parse moin wiki IDs from
    mozilla/firefox cookie database.
  * debian/control: Fix typo in "vim-addons" in long description.
    (Closes: #612706)
  * Drop Pythonic build system; this is only a single-script program, no
    modules at all, and setup.py installs too much noise around it. Just
    install the executable and manpage manually.
  * Move to debhelper level 7 and from cdbs to dh.
  * Bump Standards-Version to 3.9.1.

 -- Martin Pitt <mpitt@debian.org>  Fri, 11 Feb 2011 16:22:48 +0100

editmoin (1.15-1) unstable; urgency=low

  * New upstream release:
    - Moin used to work with numerical IDs for identification, and editmoin
      was still based on this model. This release adds support for direct
      authentication as available in current Moin releases.
    - The new file ~/.moin_users is now parsed to obtain usernames, supporting
      the feature above. Shortcuts are also supported in this file.
    - Added support for textcha question handling.
  * debian/control: Replace python-dev build dependency (which isn't really
    required) with just python.
  * Bump Standards-Version to 3.8.4 (no changes necessary).
  * Add debian/source/format. Keep 1.0 for now, 3.0 does not work well for VCS
    maintained packages.

 -- Martin Pitt <mpitt@debian.org>  Wed, 23 Jun 2010 18:17:52 +0200

editmoin (1.10.1-3) unstable; urgency=low

  * editmoin: Fix md5 deprecation warning with Python 2.6, thanks to Max
    Bowsher. (LP: #357221)
  * debian/control: Add missing misc:Depends, thanks lintian.
  * debian/control: Bump Standards-Version (no changes necessary).

 -- Martin Pitt <mpitt@debian.org>  Sun, 03 May 2009 15:17:11 +0200

editmoin (1.10.1-2) unstable; urgency=low

  * Rebuild against latest python-central. (Closes: #490460)

 -- Martin Pitt <mpitt@debian.org>  Sun, 13 Jul 2008 11:56:42 +0200

editmoin (1.10.1-1) unstable; urgency=low

  * New upstream version:
    - Merged all Debian fixes except the vim modeline addition.
    - Add moin 1.6 syntax file.
    - No other changes.
    - The upstream inclusion of the manpage now makes it fall under GPL.
      (Closes: #460187)
  * Revert our vim modeline patch, too, since there are now two different
    ones, and it looks ugly for users of other editors.
  * debian/rules: Remove simple-patchsys.mk, we use bzr branches.
  * debian/editmoin.install: Adapt for multiple moin vim syntax files.
  * Add moin-plugin.vim: auto file-type plugin for vim, to replace the former
    vim modeline hack. Install it in debian/rules.
  * Add debian/vim-editmoin.yaml, a "new vim world order" registry file for
    the vim plugin. Suggest vim-addon-manager and add a stanza about it to the
    package description.

 -- Martin Pitt <mpitt@debian.org>  Wed, 05 Mar 2008 17:53:10 +0100

editmoin (1.9.1-2) unstable; urgency=low

  * Import package into bzr, branching from upstream trunk
    (https://code.launchpad.net/~niemeyer/editmoin/trunk). Drop debian/patches
    and apply them inline, since that's much more convenient for merging.
    Add Vcs-Bzr: to debian/control.
  * debian/control: Fix typo in description. (Closes: #459549)
  * Add editmoin.1: manpage, based on former README. Install it in setup.py.
    (Closes: #453682)
  * Remove debian/editmoin.README.debian, superseded by manpage.
  * editmoin: Use subprocess instead of os.system() to run the editor, to
    avoid issues with weird $EDITOR values.
  * editmoin: Fix crash when the first line starting with "@ " comes before
    the first line starting with "@@". (LP: #190081)

 -- Martin Pitt <mpitt@debian.org>  Sat, 16 Feb 2008 20:50:45 +0100

editmoin (1.9.1-1) unstable; urgency=low

  * Initial release to Debian. (Closes: #452946)
  * debian/patches/01_vim-modeline.patch: Add a vim modeline to automatically
    enable the moin.vim syntax file.
  * debian/patches/02_http_auth.patch:
    - Replace URLopener with FancyURLopener to make HTTP authentication
      actually work. Thanks to Soren Hansen for the patch.
  * Add editmoin.README.debian: Upstream tarball does not come with any
    documentation, take the http://labix.org/editmoin Moin source and mangle
    it a bit for readability.

 -- Martin Pitt <mpitt@debian.org>  Sun, 30 Dec 2007 12:40:06 +0100
